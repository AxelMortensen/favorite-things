Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?

 Red

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
Steak

3. Who is your favorite fictional character?
too many to choose from

4. What is your favorite animal?
Cat

5. What is your favorite programming language? (Hint: You can always say Python!!)
Python, it's the only one I know
